# java-design-patterns

## 创建型模式(creational-patterns)：
	### factory	                --工厂模式
	1. abstract factory         --抽象工厂模式
	2. factory method			--工厂方法模式
	3. simple factory           --简单工厂

	### builder					--建造者模式
		适用于有多个构造函数的场景
	### prototype				--原型模式
	1. 实现java自带的cloneable接口,然后重新clone()方法，来实现对象的拷贝
	2. 通过序列化和反序列化的方式，来实现对象的拷贝 -- 推荐
	### singleton				--单例模式
	1. lazy singleton --懒汉式
	2. singleton    --饿汉式
	3. thread safe double checked locking lazy loader --线程安全-双重校验-懒汉式
	4. thread safe lazy loader --线程安全-懒汉式
	5. enum singleton --枚举类型-单例
	6. lazy initialzation holer --单例的最佳实践模式
	### --
	### property
	### multiton
	### object pool
	### --

## 结构型模式(structural-patterns)：
	* adapter					--适配器模式
	* bridge					--桥接模式
	* composite					--组合模式
	* decorator					--装饰模式
	* facade					--外观模式
	* flyweight					--亨元模式
	* proxy						--代理模式
	* --
	* Service locator
	* Servant
	* Event Aggregator
	* --
## 行为模式(behavioral-patterns)：
	* chain of responsibillty	--责任链模式
	* command					--命令模式
	* iterator					--迭代模式
	* mediatar					--中介者模式
	* memenic					--备忘录模式
	* observer					--观察者模式
	* state 					--状态模式
	* strategy					--策略模式
	* template method			--模板方法
	* visitor					--访问者模式
	* Null object
	* interpreter filter		--解释器模式
	* Specification
	* dependency injection











