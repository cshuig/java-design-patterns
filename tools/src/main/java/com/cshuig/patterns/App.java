package com.cshuig.patterns;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
//        /**
//         * 不以 /  开头的, 则从当前类所在的包路径下获取...
//         * 以   /  开头的, 则从classpath根目录下获取...
//         */
//        System.out.println(App.class.getResource(""));
//        System.out.println(App.class.getResource("/"));
//
//        /**
//         * classLoader都是从classpath根目录开始...
//         * 所以：App.class.getResource("/") = App.class.getClassLoader().getResource("")
//         */
//        System.out.println(App.class.getClassLoader().getResource(""));
//        System.out.println(App.class.getClassLoader().getResource("log.txt"));
//        try {
//            File file = new File(App.class.getClassLoader().getResource("log.txt").getPath());
//            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
//            String tempStr = null;
//            int line = 1;
//            StringBuilder stringBuilder = new StringBuilder();
//            while ((tempStr = bufferedReader.readLine()) != null) {
//                int index = tempStr.indexOf("\"notify-id\":");
//                String notifyId = tempStr.substring(index + 12, index + 17);
//                stringBuilder.append(notifyId).append(",");
//                line++;
//            }
//            String sqlstart = "update ru_notify set `status` = 'DONE' where notify_id in (";
//            System.out.println(sqlstart + stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString() + ");");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        boolean a = "ABC".equalsIgnoreCase("abc");
        System.out.printf(String.valueOf(a));

    }
}
