package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public class AuthorizationAop extends Decorator {


    public AuthorizationAop(IBusiness business) {
        super(business);
    }
    /**
     * bank save money
     * 1、admin can save
     * 2、use aop write log
     */
    @Override
    public void saveMoney(String userName, Double money) {
        if (!"admin".equals(userName)) {
            System.out.println(String.format("%s without permission", userName));
            return ;
        }
        super.saveMoney(userName, money);

    }
}
