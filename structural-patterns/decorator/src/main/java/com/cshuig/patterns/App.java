package com.cshuig.patterns;

/**
 * 层级关系如下(上层依赖下层):
 *
 * App
 * AuthorizationAop
 * LogAop
 * DefaultBusiness
 */
public class App {
    public static void main( String[] args ) {
        IBusiness business = new DefaultBusiness();
        LogAop logAop = new LogAop(business);
        AuthorizationAop authorizationAop = new AuthorizationAop(logAop);
        authorizationAop.saveMoney("user1", 10000.00D);

        authorizationAop.saveMoney("admin", 10000.00D);
    }
}
