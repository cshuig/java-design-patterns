package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public interface IBusiness {

    /**
     * bank save money
     * 1、At least 10000 dollars to save
     * 2、use aop write log
     * */
    public void saveMoney(String userName, Double money);
}
