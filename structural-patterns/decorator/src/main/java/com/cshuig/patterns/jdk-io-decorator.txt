装饰模式在jdk io 流中的应用
InputStream --超类
  ByteArrayInputStream -- 子类  被装饰者
  FileInputStream -- 子类 被装饰者

  FilterInputStream -- 子类 装饰者

  DataInputStream -- 子类 被装饰者装饰后的成品
  BufferedInputStream -- 子类 被装饰者装饰后的成品
  PushbackInputStream -- 子类 被装饰者装饰后的成品
  GZIPInputStream -- 子类 被装饰者装饰后的成品
  PushbackInputStream -- 子类 被装饰者装饰后的成品
  DigestInputStream -- 子类 被装饰者装饰后的成品

从上面及对应的源码中可以看出：InputStream就是装饰模式中的超类(Component), ByteArrayInputStream、FileInputStream就是被装饰者,
FilterInputStream就是装饰者(Decorator), 其他的类就是被装饰后的成品类
根据装配模式的特点，可以这样使用io流
File file = new File("/home/decoratorTest.txt");
FileInputStream fileInputStream = new FileInputStream(file);
BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
也可以写成BufferedInputStream bufferedInputStream = new BufferedInputStream (new FileInputStream(new File ("/home/decoratorTest.txt")));
