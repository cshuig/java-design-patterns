package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public class DefaultBusiness implements IBusiness {
    /**
     * bank save money
     * 1、At least 10000 dollars to save
     * 2、use aop write log
     */
    @Override
    public void saveMoney(String userName, Double money) {
        System.out.println(String.format("%s has perssion and save money %f", userName, money));
    }
}
