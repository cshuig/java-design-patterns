package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public abstract class Decorator implements IBusiness {

    private IBusiness business;

    public Decorator(IBusiness business) {
        this.business = business;
    }

    /**
     * bank save money
     * 1、At least 10000 dollars to save
     * 2、use aop write log
     */
    @Override
    public void saveMoney(String userName, Double money) {
        business.saveMoney(userName, money);
    }
}
