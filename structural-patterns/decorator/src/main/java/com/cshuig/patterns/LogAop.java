package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public class LogAop extends Decorator {

    public LogAop(IBusiness business) {
        super(business);
    }

    /**
     * bank save money
     * 1、At least 10000 dollars to save
     * 2、use aop write log
     */
    @Override
    public void saveMoney(String userName, Double money) {
        super.saveMoney(userName, money);
        System.out.println(String.format("Writing Log: %s saved %f", userName, money));
    }
}
