package com.cshuig.patterns;

import com.cshuig.patterns.proxy.dynamic.Target;
import com.cshuig.patterns.proxy.dynamic.TargetImpl;
import com.cshuig.patterns.proxy.dynamic.TargetInterceptor;
import com.cshuig.patterns.proxy.dynamic.interceptor.Interceptor;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Target target = new TargetImpl();
        target.execute();

        Interceptor interceptor = new TargetInterceptor();
        target = (Target) interceptor.register(target);
        target.execute();
        target.test();
    }
}
