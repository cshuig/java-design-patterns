package com.cshuig.patterns.proxy.dynamic.interceptor;

import com.cshuig.patterns.proxy.dynamic.model.Invocation;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by hogan.lin on 2016/3/25.
 *
 * 拦截器实现就是基于 动态代理
 */
public interface Interceptor {
    Object interceptor(Invocation invocation) throws InvocationTargetException, IllegalAccessException;
    Object register(Object target);
}
