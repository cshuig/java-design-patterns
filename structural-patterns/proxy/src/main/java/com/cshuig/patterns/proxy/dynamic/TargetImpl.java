package com.cshuig.patterns.proxy.dynamic;

/**
 * Created by hogan.lin on 2016/3/25.
 */
public class TargetImpl implements Target {

    @Override
    public void execute() {
        System.out.println("Hi I`m " + TargetImpl.class.getName());
    }

    @Override
    public void test() {
        System.out.println("我没有被拦截:test()");
    }
}
