package com.cshuig.patterns.proxy.dynamic;

/**
 * Created by hogan.lin on 2016/3/25.
 */
public interface Target {
    void execute();
    void test();
}
