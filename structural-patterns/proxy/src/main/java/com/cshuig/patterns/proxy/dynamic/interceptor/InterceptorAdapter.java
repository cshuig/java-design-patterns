package com.cshuig.patterns.proxy.dynamic.interceptor;

import com.cshuig.patterns.proxy.dynamic.ProxyClass;
import com.cshuig.patterns.proxy.dynamic.model.Invocation;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by hogan.lin on 2016/3/25.
 */
public abstract class InterceptorAdapter implements Interceptor {

    @Override
    public Object interceptor(Invocation invocation) throws InvocationTargetException, IllegalAccessException {
        preHandle();    // 方法执行前
        Object obj = invocation.invoke();
        postHandle();   // 方法执行后
        //还可以定义一个异常处理函数
        return obj;
    }

    @Override
    public Object register(Object target) {
        return ProxyClass.bind(target, this);
    }

    /**
     * 方法执行前的执行
     */
    public void preHandle() {
    }

    /**
     * 方法执行后处理
     */
    public void postHandle() {
    }
}
