package com.cshuig.patterns.proxy.dynamic;

import com.cshuig.patterns.proxy.dynamic.annotation.IntercepterMethod;
import com.cshuig.patterns.proxy.dynamic.interceptor.InterceptorAdapter;

/**
 * Created by hogan.lin on 2016/3/25.
 */
@IntercepterMethod("execute")
public class TargetInterceptor extends InterceptorAdapter {

    @Override
    public void preHandle() {
        System.out.println();
        System.out.println("--------------------------");
        System.out.println("拦截器前置方法执行....");
    }

    @Override
    public void postHandle() {
        System.out.println("拦截器后置方法执行....");
        System.out.println("--------------------------");
        System.out.println();
    }
}
