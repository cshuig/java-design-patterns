package com.cshuig.patterns.InterfaceMembersAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class RedisCacheManager implements Cache {
    private Cache cache;

    public RedisCacheManager() {
        cache = new RedisCache();
    }
    @Override
    public void put() {
        cache.put();
    }

    @Override
    public void get() {
        cache.get();
    }

    @Override
    public void clear() {
        cache.clear();
    }
}
