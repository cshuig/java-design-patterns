package com.cshuig.patterns.DefaultAdapter;

/**
 * Created by hogan on 2015/8/14.
 *
 * 默认的适配器模式：
 *  当我们想实现某个接口，但又不想实现接口的全部方法，而只想实现其中的部分接口时，
 *  可以使用一个抽象的类，这个抽象类实现接口的所有方法,而具体的接口实现类只需要重新覆盖部分方法即可
 *  这个抽象类 处 在 接口 和接 口实现类 中间,
 */
public class DefaultAdppterSkillImpl extends AbstractDefaultSkill {
    @Override
    public void speakEnglish() {
        System.out.println("我只会将英语");
    }
}
