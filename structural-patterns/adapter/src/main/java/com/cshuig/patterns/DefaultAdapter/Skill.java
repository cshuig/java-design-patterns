package com.cshuig.patterns.DefaultAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public interface Skill {
    public void speakEnglish();
    public void speakChina();
    public void speakLtalian();
}
