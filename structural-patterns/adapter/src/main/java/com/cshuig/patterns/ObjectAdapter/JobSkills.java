package com.cshuig.patterns.ObjectAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public interface JobSkills {
    public void speakEnglish();
    public void speakChina();
    public void speakLtalian();
}
