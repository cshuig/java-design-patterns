package com.cshuig.patterns.InterfaceMembersAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class App {
    public static void main(String[] args) {
        Cache cache = new RedisCacheManager();
        cache.put();
        cache.get();
        cache.clear();
    }
}
