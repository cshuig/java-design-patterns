package com.cshuig.patterns.InterfaceMembersAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class RedisCache implements Cache {

    @Override
    public void put() {
        System.out.println("redis put");
    }

    @Override
    public void get() {
        System.out.println("redis get");
    }

    @Override
    public void clear() {
        System.out.println("redis clear");
    }
}
