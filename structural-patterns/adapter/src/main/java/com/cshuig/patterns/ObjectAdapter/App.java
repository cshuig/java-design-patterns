package com.cshuig.patterns.ObjectAdapter;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        JobSkills jobSkills = new ObjectAdapterSkillImpl(new Person());
        jobSkills.speakChina();
        jobSkills.speakEnglish();
        jobSkills.speakLtalian();
    }
}
