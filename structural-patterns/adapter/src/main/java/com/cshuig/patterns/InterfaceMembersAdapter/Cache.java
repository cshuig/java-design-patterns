package com.cshuig.patterns.InterfaceMembersAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public interface Cache {
    public void put();
    public void get();
    public void clear();
}
