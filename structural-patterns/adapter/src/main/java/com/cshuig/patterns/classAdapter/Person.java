package com.cshuig.patterns.classAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class Person {
    public void speakEnglish() {
        System.out.println("I Can Speak English");
    }

    public void speakChina() {
        System.out.println("我会说汉语");
    }
}
