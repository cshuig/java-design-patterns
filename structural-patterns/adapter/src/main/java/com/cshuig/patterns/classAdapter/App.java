package com.cshuig.patterns.classAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class App {
    public static void main(String[] args) {
        JobSkills jobSkills = new ClassAdapter();
        jobSkills.speakChina();
        jobSkills.speakEnglish();
        jobSkills.speakLtalian();
    }
}
