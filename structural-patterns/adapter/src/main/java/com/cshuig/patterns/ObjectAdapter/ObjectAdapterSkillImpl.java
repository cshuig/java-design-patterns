package com.cshuig.patterns.ObjectAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class ObjectAdapterSkillImpl implements JobSkills {
    private Person person;

    public ObjectAdapterSkillImpl(Person _person) {
        this.person = _person;
    }

    @Override
    public void speakEnglish() {
        person.speakEnglish();
    }

    @Override
    public void speakChina() {
        person.speakChina();
    }

    @Override
    public void speakLtalian() {
        System.out.println("I Can Speak Ltalian");
    }
}
