package com.cshuig.patterns.DefaultAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public abstract class AbstractDefaultSkill implements Skill {
    @Override
    public void speakEnglish() {

    }

    @Override
    public void speakChina() {

    }

    @Override
    public void speakLtalian() {

    }
}
