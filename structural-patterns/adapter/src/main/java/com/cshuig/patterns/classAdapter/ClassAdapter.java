package com.cshuig.patterns.classAdapter;

/**
 * Created by hogan on 2015/8/14.
 * 在不破坏Person类的情况下，将person类适配到目标类中，以达到目录想用的技能
 * person中的方法 和 接口中的方法一样，所有适配器类继承Person类后，等于用person中的方法来默认重写目标接口类中的方法
 * 此时适配器类只需要扩展person没有的功能
 */
public class ClassAdapter extends Person implements JobSkills {

    @Override
    public void speakLtalian() {
        System.out.println("I Can Speak Ltalian");
    }
}
