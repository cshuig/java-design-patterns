package com.cshuig.patterns.DefaultAdapter;

/**
 * Created by hogan on 2015/8/14.
 */
public class App {
    public static void main(String[] args) {
        Skill skill = new DefaultAdppterSkillImpl();
        skill.speakChina();
        skill.speakEnglish();
        skill.speakLtalian();
    }
}
