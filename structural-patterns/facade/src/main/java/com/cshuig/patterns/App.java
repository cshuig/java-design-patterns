package com.cshuig.patterns;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        // 对外的统一接口,完全隐藏了内部复杂的子系统结构
        WorkFacade workFacade = new WorkFacade();
        workFacade.doSometing();
    }
}
