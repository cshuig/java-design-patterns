package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public class Unity3DProgrammer extends Programmer {
    @Override
    public void work() {
        System.out.println(showActionName() + " doing game develop and test");
    }

    @Override
    public String showActionName() {
        return this.getClass().getName();
    }
}
