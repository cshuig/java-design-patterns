package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public abstract class Programmer {

    public void wakeUp() {
        System.out.println(showActionName() + " wake up");
    }

    public void goToWork() {
        System.out.println(showActionName() + " go to work");
    }

    public void offDuty() {
        System.out.println(showActionName() + " off duty");
    }

    public void goHome() {
        System.out.println(showActionName() + " go home");
    }

    public void goToSleep() {
        System.out.println(showActionName() + " go to sleep");
    }

    public void doAction(Action action) {
        switch (action) {
            case WAKE_UP:
                wakeUp();
                break;
            case GO_T0_WORK:
                goToWork();
                break;
            case WORK:
                work();
                break;
            case OFF_DUTY:
                offDuty();
                break;
            case GO_HOME:
                goHome();
                break;
            case GO_TO_SLEEP:
                goToSleep();
                break;
            default:
                System.out.println("unknow action " + action);
        }
    }

    public void doAction(Action... actions) {
        for (Action action : actions) {
            doAction(action);
        }
    }
    public abstract void work();
    public abstract String showActionName();

    static enum Action {
        WAKE_UP, GO_T0_WORK, WORK, OFF_DUTY, GO_HOME, GO_TO_SLEEP
    }
}
