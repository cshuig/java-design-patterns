package com.cshuig.patterns;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hogan on 2015/9/16.
 * 客户端只能和这个类打交道，不能和这个类的内部业务打交道(如直接和JavaProgrammer等通信)
 * 好处是：这些功能结合起来构成的子系统, 对外只需要一个统一接口操作就可以完成想要的工作
 * 这就是 外观模式
 */
public class WorkFacade {

    private static List<Programmer> works;

    static {
        works = new ArrayList<>();
        works.add(new JavaProgrammer());
        works.add(new UIProgrammer());
        works.add(new Unity3DProgrammer());
        works.add(new DBProgrammer());
    }

    public void doSometing() {
        startNewDay();
        doWorks();
        endDay();
    }
    public void startNewDay() {
        makeAction(Programmer.Action.WAKE_UP, Programmer.Action.GO_T0_WORK);
    }

    public void doWorks() {
        makeAction(Programmer.Action.WORK, Programmer.Action.OFF_DUTY);
    }

    public void endDay() {
        makeAction(Programmer.Action.GO_HOME, Programmer.Action.GO_TO_SLEEP);
    }
    public void makeAction(Programmer.Action...actions) {
        for (Programmer programmer : works) {
            programmer.doAction(actions);
        }
    }

}
