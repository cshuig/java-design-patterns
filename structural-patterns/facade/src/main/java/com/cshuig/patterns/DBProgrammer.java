package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public class DBProgrammer extends Programmer {

    @Override
    public void work() {
        System.out.println(showActionName() + " doning backup db data opration");
    }

    @Override
    public String showActionName() {
        return "DBProgrammer";
    }
}
