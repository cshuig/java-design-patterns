package com.cshuig.patterns;

/**
 * Created by hogan on 2015/9/16.
 */
public class UIProgrammer extends Programmer {
    @Override
    public void work() {
        System.out.println(showActionName() + " doing draw for game");
    }

    @Override
    public String showActionName() {
        return this.getClass().getName();
    }
}
