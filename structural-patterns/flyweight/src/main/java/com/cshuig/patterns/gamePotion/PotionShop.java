package com.cshuig.patterns.gamePotion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hogan on 2015/11/12.
 */
public class PotionShop implements Shop {
    private List<Potion> topShelf;
    private List<Potion> bottomShelf;

    public PotionShop() {
        topShelf = new ArrayList<>();
        bottomShelf = new ArrayList<>();
        fillShelf();
    }
    @Override
    public void fillShelf() {
        topShelf.add(PotionFactory.getPotion(PotionType.HEALING));
        topShelf.add(PotionFactory.getPotion(PotionType.HEALING));

        topShelf.add(PotionFactory.getPotion(PotionType.HOLY_WATER));
        topShelf.add(PotionFactory.getPotion(PotionType.HOLY_WATER));
        topShelf.add(PotionFactory.getPotion(PotionType.HOLY_WATER));

        topShelf.add(PotionFactory.getPotion(PotionType.INVISIBILITY));
        topShelf.add(PotionFactory.getPotion(PotionType.INVISIBILITY));
        topShelf.add(PotionFactory.getPotion(PotionType.INVISIBILITY));

        bottomShelf.add(PotionFactory.getPotion(PotionType.POISON));
        bottomShelf.add(PotionFactory.getPotion(PotionType.POISON));

        bottomShelf.add(PotionFactory.getPotion(PotionType.STRENGTH));
        bottomShelf.add(PotionFactory.getPotion(PotionType.STRENGTH));
        bottomShelf.add(PotionFactory.getPotion(PotionType.STRENGTH));
    }

    @Override
    public void showShelf() {
        System.out.println("开始 show PotionShop.showShelf");
        for (Potion potion : topShelf) {
            potion.drink();
        }
        System.out.println("结束 show PotionShop.showShelf");

        System.out.println("\r");

        System.out.println("开始 show PotionShop.bottomShelf");
        for (Potion potion : bottomShelf) {
            potion.drink();
        }
        System.out.println("结束 show PotionShop.bottomShelf");
    }
}
