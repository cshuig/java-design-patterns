package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/11/12.
 */
public class InvisibilityPotion implements Potion {
    @Override
    public void drink() {
        System.out.println("InvisibilityPotion.drink --- " + System.identityHashCode(this));
    }
}
