package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/11/12.
 */
public class HolyWaterPotion implements Potion {
    @Override
    public void drink() {
        System.out.println("HolyWaterPotion.drink --- " + System.identityHashCode(this));
    }
}
