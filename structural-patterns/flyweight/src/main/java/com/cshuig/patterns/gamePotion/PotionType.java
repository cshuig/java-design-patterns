package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/9/21.
 */
public enum PotionType {

    HEALING(),
    INVISIBILITY,
    STRENGTH,
    HOLY_WATER,
    POISON;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
