package com.cshuig.patterns.gamePotion;

import java.util.EnumMap;
import java.util.Map;

/**
 * Created by hogan on 2015/11/12.
 */
public class PotionFactory {

    private static PotionFactory potionFactory = new PotionFactory();
    private static Map<PotionType, Potion> cache;
    private PotionFactory(){
        cache = new EnumMap<>(PotionType.class);
    }

    public static Potion getPotion(PotionType type) {
        Potion potion = cache.get(type);
        if (potion == null) {
            switch (type) {
                case HEALING:
                    cache.put(type, new HealingPotion());
                    break;
                case INVISIBILITY:
                    cache.put(type, new InvisibilityPotion());
                    break;
                case STRENGTH:
                    cache.put(type, new StrengthPotion());
                    break;
                case HOLY_WATER:
                    cache.put(type, new HolyWaterPotion());
                    break;
                case POISON:
                    cache.put(type, new PoisonPotion());
                    break;
                default:
                    break;
            }
            potion = cache.get(type);
        }
        return potion;
    }


}
