package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/11/12.
 */
public class PoisonPotion implements Potion {
    @Override
    public void drink() {
        System.out.println("PoisonPotion.drink --- " + System.identityHashCode(this));
    }
}
