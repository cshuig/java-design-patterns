package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/9/21.
 */
public interface Potion {

    void drink();
}
