package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/11/12.
 */
public class StrengthPotion implements Potion {
    @Override
    public void drink() {
        System.out.println("StrengthPotion.drink --- " + System.identityHashCode(this));
    }
}
