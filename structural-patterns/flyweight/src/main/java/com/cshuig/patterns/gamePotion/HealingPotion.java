package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/11/12.
 */
public class HealingPotion implements Potion {
    @Override
    public void drink() {
        System.out.println("HealingPotion.drink ---" + System.identityHashCode(this));
    }
}
