package com.cshuig.patterns.dataPool;

/**
 * Created by hogan on 2015/9/18.
 */
public class App {
    public static void main(String[] args) {

        for (int i = 0; i < 1000; i++) {
            QueryThread queryThread = new QueryThread();
            Thread thread = new Thread(queryThread);
            thread.start();
        }

    }

    static class QueryThread implements Runnable {
        @Override
        public void run() {
            try {
                //程序每次都是从数据库连接池中获取，已经建立连接的对象.
                // 这样的好处是不必每次查数据库都独立建立一个连接对象, 可以有效的提高程序的性能
                DatabaseConnect connect = DatabasePoolFactory.getInstance().getDatapool();
                while (connect == null) {
                    System.out.println(Thread.currentThread().getName() + " === " + System.currentTimeMillis() + " === 线程池用完睡眠5秒");
                    Thread.sleep(5000);
                    connect = DatabasePoolFactory.getInstance().getDatapool();
                }
                System.out.println(Thread.currentThread().getName() + " === " + connect.toString());
                DatabasePoolFactory.getInstance().releasePool(connect);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
