package com.cshuig.patterns.gamePotion;

/**
 * Created by hogan on 2015/11/12.
 */
public interface Shop {

    /**
     * 填充货架上的药水
     */
    void fillShelf();

    /**
     * 展示货架上的药水
     */
    void showShelf();
}
