package com.cshuig.patterns.dataPool;

/**
 * Created by hogan on 2015/9/18.
 */
public class DatabaseConnect {
    private String name;

    public DatabaseConnect(String databaseName) {
        this.name = databaseName;
    }

    @Override
    public String toString() {
        return "DatabaseConnect{" +
                "name='" + name + " __ " + System.identityHashCode(this) + '}';
    }
}
