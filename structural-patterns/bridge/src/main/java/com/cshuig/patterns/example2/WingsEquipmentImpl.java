package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 翅膀实现接口的扩展，扩展独有的功能 -- 飞行
 */
public abstract  class WingsEquipmentImpl extends EquipmentImpl {

    /** 飞行 */
    public abstract void flying();
}
