package com.cshuig.patterns.example1;

/**
 * Created by hogan on 2015/8/17.
 */
public class EmailMessageImpl extends Message {
    @Override
    public void send(String msg, String user) {
        System.out.println("发送Email信息：[" + msg + "]给[" + user + "]");
    }
}
