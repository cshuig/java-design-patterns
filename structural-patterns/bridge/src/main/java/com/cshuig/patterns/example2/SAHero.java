package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 隐形刺客
 */
public class SAHero extends ShoesEquipmentImpl {
    /**
     * 移动速度
     */
    @Override
    public void movementSpeed() {
        System.out.println("隐形刺客用鞋子加快移动速度");
    }

    /**
     * 装备类型
     */
    @Override
    public void type() {
        System.out.println("我的装备类型是：鞋子");
    }

    /**
     * 穿上
     */
    @Override
    public void putOn() {
        System.out.println("隐形刺客戴上了装备: 鞋子");
    }

    /**
     * 卸下
     */
    @Override
    public void unload() {
        System.out.println("隐形刺客戴上了装备: 鞋子");
    }
}
