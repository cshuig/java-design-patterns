package com.cshuig.patterns.example1;

/**
 * Created by hogan on 2015/8/17.
 */
public class MoblieMessageImpl extends Message {
    @Override
    public void send(String msg, String user) {
        System.out.println("发送mobile信息：[" + msg + "]给[" + user + "]");
    }

    /**
     * 扩展自己独有的功能
     * @param mobile
     */
    public void validMobile(Integer mobile) {

    }
}
