package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 装备实现接口
 * 此时：和抽象的Equipment部分是分离，独立开
 */
public abstract class EquipmentImpl {

    /** 装备类型 */
    public abstract void type();

    /** 穿上 */
    public abstract void putOn();

    /** 卸下 */
    public abstract void unload();

}
