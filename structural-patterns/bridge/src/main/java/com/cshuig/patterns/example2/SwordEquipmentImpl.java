package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 剑实现接口的扩展，扩展独有的功能 -- 攻击
 */
public abstract  class SwordEquipmentImpl extends EquipmentImpl {

    /** 攻击 */
    public abstract void attack();
}
