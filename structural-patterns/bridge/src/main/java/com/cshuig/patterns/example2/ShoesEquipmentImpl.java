package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 鞋子实现接口的扩展，扩展独有的功能 -- 移动速度
 */
public abstract  class ShoesEquipmentImpl extends EquipmentImpl {

    /** 移动速度 */
    public abstract void movementSpeed();
}
