package com.cshuig.patterns.example2;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        SwordEquipment swordEquipment = new SwordEquipment(new BlademasterHero());
        swordEquipment.type();
        swordEquipment.putOn();
        swordEquipment.attack();
        swordEquipment.unload();
        System.out.println();

        ShoesEquipment shoesEquipment = new ShoesEquipment(new SAHero());

        shoesEquipment.type();
        shoesEquipment.putOn();
        shoesEquipment.movementSpeed();
        shoesEquipment.unload();
        System.out.println();

        WingsEquipment wingsEquipment = new WingsEquipment(new ViperHero());
        wingsEquipment.type();
        wingsEquipment.putOn();
        wingsEquipment.flying();
        wingsEquipment.unload();
        System.out.println();
    }
}
