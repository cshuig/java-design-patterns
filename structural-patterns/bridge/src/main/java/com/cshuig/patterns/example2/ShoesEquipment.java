package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 鞋子抽象实现类
 */
public class ShoesEquipment extends Equipment {


    public ShoesEquipment(EquipmentImpl equipmentImpl) {
        super(equipmentImpl);
    }

    @Override
    public ShoesEquipmentImpl getImpl() {
        return (ShoesEquipmentImpl) equipmentImpl;
    }

    public void movementSpeed() {
        this.getImpl().movementSpeed();
    }
    /**
     * 装备类型
     */
    @Override
    public void type() {
        this.getImpl().type();
    }

    /**
     * 穿上
     */
    @Override
    public void putOn() {
        this.getImpl().putOn();
    }

    /**
     * 卸下
     */
    @Override
    public void unload() {
        this.getImpl().unload();
    }
}
