package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 英雄剑圣: Blademaster
 */
public class BlademasterHero extends SwordEquipmentImpl {
    /**
     * 攻击
     */
    @Override
    public void attack() {
        System.out.println("剑圣用剑攻击: 怪物");
    }

    /**
     * 装备类型
     */
    @Override
    public void type() {
        System.out.println("我的装备类型是：剑");
    }

    @Override
    public void putOn() {
        System.out.println("剑圣戴上了装备: 剑");
    }

    @Override
    public void unload() {
        System.out.println("剑圣卸下了装备: 剑");
    }
}
