package com.cshuig.patterns.example1;

/**
 * Created by hogan on 2015/8/17.
 */
public class App {
    public static void main(String[] args) {
        Message siteInternal = new SiteInternalMessageImpl();
        Message email = new EmailMessageImpl();

        //使用标准normal方式发送站内信息
        AbstractMessage abstractMessage = new NormalMessageImpl(siteInternal);
        abstractMessage.sendMsg("小伙子你涨薪资了,100块,高兴起来吧", "张飞");

        //使用紧急urgent方式，发送email信息
        abstractMessage = new UrgentMessageImpl(email);
        abstractMessage.sendMsg("青岛码头核爆炸，悲哀", "全国人打");

        //扩展，如使用紧急urgent发送手机短信，此时需要新增一个消息发送实现类：MoblieMessageImpl,就可以搞定
        Message mobile = new MoblieMessageImpl();
        abstractMessage = new UrgentMessageImpl(mobile);
        abstractMessage.sendMsg("小妹妹，你六合彩中了, 快来领吧，地址东莞...", "优衣库女主角");
    }
}
