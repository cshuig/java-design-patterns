package com.cshuig.patterns.example1;

/**
 * Created by hogan on 2015/8/17.
 */
public class NormalMessageImpl extends AbstractMessage {
    public NormalMessageImpl(Message messageImpl) {
        super(messageImpl);
    }

    @Override
    public void sendMsg(String msg, String user) {
        System.out.printf("使用normal方式: ");
        super.sendMsg(msg, user);
    }
}
