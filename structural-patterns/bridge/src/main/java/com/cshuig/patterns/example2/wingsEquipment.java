package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 翅膀
 */
public class WingsEquipment extends Equipment {


    public WingsEquipment(EquipmentImpl equipmentImpl) {
        super(equipmentImpl);
    }

    @Override
    public WingsEquipmentImpl getImpl() {
        return (WingsEquipmentImpl) equipmentImpl;
    }

    public void flying() {
        this.getImpl().flying();
    }
    /**
     * 装备类型
     */
    @Override
    public void type() {
        this.getImpl().type();
    }

    /**
     * 穿上
     */
    @Override
    public void putOn() {
        this.getImpl().putOn();
    }

    /**
     * 卸下
     */
    @Override
    public void unload() {
        this.getImpl().unload();
    }
}
