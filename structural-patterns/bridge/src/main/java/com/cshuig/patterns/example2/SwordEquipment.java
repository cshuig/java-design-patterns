package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 双手剑
 */
public class SwordEquipment extends Equipment {


    public SwordEquipment(EquipmentImpl equipmentImpl) {
        super(equipmentImpl);
    }

    @Override
    public SwordEquipmentImpl getImpl() {
        return (SwordEquipmentImpl) equipmentImpl;
    }

    public void attack() {
        this.getImpl().attack();
    }
    /**
     * 装备类型
     */
    @Override
    public void type() {
        this.getImpl().type();
    }

    /**
     * 穿上
     */
    @Override
    public void putOn() {
        this.getImpl().putOn();
    }

    /**
     * 卸下
     */
    @Override
    public void unload() {
        this.getImpl().unload();
    }
}
