package com.cshuig.patterns.example1;

/**
 * Created by hogan on 2015/8/17.
 */
public abstract class AbstractMessage {
    private Message messageImpl;

    public AbstractMessage(Message messageImpl) {
        this.messageImpl = messageImpl;
    }

    public void sendMsg(String msg, String user) {
        messageImpl.send(msg, user);
    }
}
