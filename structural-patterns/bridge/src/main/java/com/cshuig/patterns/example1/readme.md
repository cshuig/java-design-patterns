实例一、
系统发生消息功能：
1、消息的类型有暂时会有：系统内的消息(a)、邮件消息(b)。 需要考虑的是：将来有可能会扩展，如添加: 手机短信发送消息(d)
2、消息等级划分：普通(A)、紧急(B)、特级(C)，将来还有可能添加: 十万火急(D)
3、用前两点进行组合：Aa、Ab、Ba、Bb、Ca、Cc、Da、Db
传统实现方式：
    定义一个发送消息的接口类：Massage， 里面一个方法 send()
    Aa implements Massage
    Ab implements Massage
    Ba implements Massage
    Bb implements Massage   如果中间需要扩展独有的功能，则需要扩展出一个接口 BbMassage然后 Bb implements BbMassage extends Massage
    Ca implements Massage
    Cb implements Massage
    如果还有其他的组合，需要一直这样扩展下去.....
缺少非常明显

使用桥接模式：
    期望设计出一种横向和纵向都非常独立自由且容易组合的方式如下
    纵向
    .
    .
    D     /
    C    /
    B   /
    A  /
      a  b  c . . . 横向

    上图可以看出，横向和纵向可以自由的组合使用
    抽象一个消息发送类：AbstractMessage,
    里面有一个 消息发送接口(or 抽象)类的的成员变量: private Message messageImpl; //接收的是具体消息发送实现类中的一个
    还有一个发送消息的方法sendMessage()
    桥接模式的元素说明：
    抽象部分：AbstractMessage
    实现部分：private Message messageImpl

    具体实现类：
    a implements Message , -- 系统内的消息发送
    b implements Message , -- 邮件消息发送
    抽象实现类：
    A extends AbstractMessage
    B extends AbstractMessage
    Client：

        A aA = new A(a);    --注入系统内的消息(a)，结果：发送普通的系统内的消息
        A aB = new A(b);    --注入邮件消息(b)，结果：发送普通的邮件消息

        B ba = new B(a);    --注入系统内的消息(a)，结果：发送紧急的系统内的消息
        A aB = new A(b);    --注入邮件消息(b)，结果：发送紧急的邮件消息

