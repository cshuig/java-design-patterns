package com.cshuig.patterns.example1;

/**
 * Created by hogan on 2015/8/17.
 */
public abstract class Message {
    public abstract void send(String msg, String user);
}
