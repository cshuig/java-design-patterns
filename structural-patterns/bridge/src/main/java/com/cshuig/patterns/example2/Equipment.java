package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 装备
 */
public abstract class Equipment {
    protected EquipmentImpl equipmentImpl;

    public Equipment(EquipmentImpl equipmentImpl) {
        this.equipmentImpl = equipmentImpl;
    }

    public EquipmentImpl getImpl() {
        return equipmentImpl;
    }

    /** 装备类型 */
    public abstract void type();

    /** 穿上 */
    public abstract void putOn();

    /** 卸下 */
    public abstract void unload();
}
