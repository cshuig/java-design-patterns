package com.cshuig.patterns.example2;

/**
 * Created by hogan on 2015/8/18.
 * 冥界亚龙
 */
public class ViperHero extends WingsEquipmentImpl {
    /**
     * 飞行
     */
    @Override
    public void flying() {
        System.out.println("冥界亚龙用翅膀飞起来了");
    }

    /**
     * 装备类型
     */
    @Override
    public void type() {
        System.out.println("我的装备类型是：翅膀");
    }

    /**
     * 穿上
     */
    @Override
    public void putOn() {
        System.out.println("冥界亚龙戴上了装备: 翅膀");
    }

    /**
     * 卸下
     */
    @Override
    public void unload() {
        System.out.println("冥界亚龙戴上了装备: 翅膀");
    }
}
