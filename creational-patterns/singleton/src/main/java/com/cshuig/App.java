package com.cshuig;

import com.cshuig.singleton.EnumSingleton;
import com.cshuig.singleton.LazyInitializationHoler;
import com.cshuig.singleton.LazySingleton;
import com.cshuig.singleton.Singleton;
import com.cshuig.singleton.ThreadSafeDoubleCheckedLockingLazyLoader;
import com.cshuig.singleton.ThreadSafeLazyLoader;

/**
 * Created by cshuig on 15/6/22.
 */
public class App {

    public static void main(String[] args) {

        //饿汉式
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();
        System.out.println("singletion1=" + singleton1);
        System.out.println("singletion2=" + singleton2);

        //线程不安全的 懒汉式
        LazySingleton lazySingleton1 = LazySingleton.getInstance();
        LazySingleton LazySingleton2 = LazySingleton.getInstance();
        System.out.println("lazySingleton1=" + lazySingleton1);
        System.out.println("LazySingleton2=" + LazySingleton2);

        //线程安全的 懒汉式
        ThreadSafeLazyLoader threadSafeLazyLoader1 = ThreadSafeLazyLoader.getInstance();
        ThreadSafeLazyLoader threadSafeLazyLoader2 = ThreadSafeLazyLoader.getInstance();
        System.out.println("threadSafeLazyLoader1=" + threadSafeLazyLoader1);
        System.out.println("threadSafeLazyLoader2=" + threadSafeLazyLoader2);

        //双重检查加锁
        ThreadSafeDoubleCheckedLockingLazyLoader threadSafeDoubleCheckedLockingLazyLoader1 = ThreadSafeDoubleCheckedLockingLazyLoader.getInstance();
        ThreadSafeDoubleCheckedLockingLazyLoader threadSafeDoubleCheckedLockingLazyLoader2 = ThreadSafeDoubleCheckedLockingLazyLoader.getInstance();
        System.out.println("threadSafeDoubleCheckedLockingLazyLoader1=" + threadSafeDoubleCheckedLockingLazyLoader1);
        System.out.println("threadSafeDoubleCheckedLockingLazyLoader2=" + threadSafeDoubleCheckedLockingLazyLoader2);

        EnumSingleton enumSingleton1 = EnumSingleton.INSTANCE;
        EnumSingleton enumSingleton2 = EnumSingleton.INSTANCE;
        System.out.println("enumSingleton1=" + enumSingleton1);
        System.out.println("enumSingleton2=" + enumSingleton2);

        // 单例模式的 最佳实践
        LazyInitializationHoler lazyInitializationHoler1 = LazyInitializationHoler.getInstance();
        LazyInitializationHoler lazyInitializationHoler2 = LazyInitializationHoler.getInstance();
        System.out.println("lazyInitializationHoler1=" + lazyInitializationHoler1);
        System.out.println("lazyInitializationHoler2=" + lazyInitializationHoler2);
    }
}
