package com.cshuig.singleton;

/**
 * Created by cshuig on 15/6/22.
 * 饿汉式
 */
public class Singleton {

    private static Singleton singleton = new Singleton();
    private Singleton() {
        System.out.println("饿汉式实例初始化");
    }

    public static Singleton getInstance() {
        return singleton;
    }
}
