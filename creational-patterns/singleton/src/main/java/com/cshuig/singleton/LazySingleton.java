package com.cshuig.singleton;

/**
 * Created by cshuig on 15/6/22.
 * 懒汉式
 */
public class LazySingleton {

    private static LazySingleton lazySingleton = null;
    
    private LazySingleton() {
        System.out.println("Lazy Singleton init");
    }

    public static LazySingleton getInstance() {
        if (lazySingleton == null) lazySingleton = new LazySingleton();
        return lazySingleton;
    }

}
