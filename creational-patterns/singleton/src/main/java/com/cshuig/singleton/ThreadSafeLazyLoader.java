package com.cshuig.singleton;

/**
 * Created by cshuig on 15/6/22.
 * 线程安全的单例延迟初始化, 需要同步块,关键字synchronized
 */
public class ThreadSafeLazyLoader {

    private static ThreadSafeLazyLoader threadSafeLazyLoader = null;

    private ThreadSafeLazyLoader() {
        System.out.println("ThreadSafeLazyLoader init");
    }

    /**
     * 这种效率比较低
     * @return
     */
    public synchronized static ThreadSafeLazyLoader getInstance() {
        if (threadSafeLazyLoader == null) {
            threadSafeLazyLoader = new ThreadSafeLazyLoader();
        }
        return threadSafeLazyLoader;
    }

}
