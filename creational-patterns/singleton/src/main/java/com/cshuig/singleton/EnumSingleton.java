package com.cshuig.singleton;

/**
 * Created by cshuig on 15/6/22.
 * java中的枚举类型：实质上就是单例
 */
public enum  EnumSingleton {

    INSTANCE;

    @Override
    public String toString() {
        return getDeclaringClass().getCanonicalName() + "@" + hashCode();
    }
}
