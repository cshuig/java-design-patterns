package com.cshuig.singleton;

/**
 * Created by cshuig on 15/6/22.
 * 这种方式好处：即满足延迟初始化、线程安全, 又能满足饿汉式的效率， 不需要锁机制, 具体的锁由虚拟机控制
 */
public class LazyInitializationHoler {

    private LazyInitializationHoler() {
        System.out.println("LazyInitializationHoler init");
    }

    /**
     * 采用类级别的内部类，能够让类加载的时候不去初始化对象, 在这个内部类里面去创建对象实例, 这样只要不使用这个内部类, 就不会创建对象实例, 从而能够同时实现延迟加载和线程安全
     */
    private static class HelperHolder {
        public static final LazyInitializationHoler LAZY_INITIALIZATION_HOLER = new LazyInitializationHoler();
    }

    public static LazyInitializationHoler getInstance() {
        return HelperHolder.LAZY_INITIALIZATION_HOLER;
    }

}
