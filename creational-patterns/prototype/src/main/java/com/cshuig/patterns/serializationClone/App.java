package com.cshuig.patterns.serializationClone;

import java.util.Date;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        Report report = new Report();
        Attachment attachment = new Attachment();
        attachment.setName("附件名字");
        attachment.setSize(10000);
        report.setName("报表名字");
        report.setDate(new Date());
        report.setAttachment(attachment);

        Report reportCopy = report.deepClone();

        System.out.println("report = reprotCopy : " + (reportCopy == report));
        System.out.println("attachment = attachmentCopy : " + (reportCopy.getAttachment() == report.getAttachment()));
    }
}
