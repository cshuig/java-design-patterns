package com.cshuig.patterns.javaApiClone;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        American american = new JacksonAmerican("jackson", new ArrayList<>(Arrays.asList("old-value")));
        Chinese chinese = new LiKuiChinese("李逵", new ArrayList<>(Arrays.asList("old-value")));

        American americanCopy;  // have deep copy
        Chinese chineseCopy;    // not hava deep copy

        HumanFactory factory = new HumanFactoryImpl(american, chinese);
        americanCopy = factory.createAmerican();
        chineseCopy = factory.createChinese();


        System.out.println("---start---深度拷贝test-----");
        System.out.println(americanCopy);   // output = jackson
        System.out.println(americanCopy.getList()); // output = old-value
        americanCopy.getList().add("add-new-value");
        System.out.println(americanCopy.getList()); // output = old-value, add-new-value
        //看是否有影响到 原型对象的list值, 事实证明：JacksonAmerican实现了 深度拷贝
        System.out.println("old american list value : " + american.getList());  // output = old-value
        System.out.println("---end---深度拷贝test-----");

        System.out.println();

        System.out.println("---start---浅度拷贝test-----");
        System.out.println(chineseCopy);    // output = 李逵
        System.out.println(chineseCopy.getList());  // output = old-value
        //修改拷贝后的对象中的list值
        chineseCopy.getList().add("add-new-value");
        System.out.println(chineseCopy.getList());  // output = old-value, add-new-value
        //看是否有影响到 原型对象的list值, 事实证明：LiKuiChinese只实现了 浅度拷贝, 对象内部的list只是拷贝了其引用地址，两者共享list内存地址
        System.out.println("old chinese list value : " + chinese.getList());  // output = old-value, add-new-value
        System.out.println("---end---浅度拷贝test-----");

    }
}
