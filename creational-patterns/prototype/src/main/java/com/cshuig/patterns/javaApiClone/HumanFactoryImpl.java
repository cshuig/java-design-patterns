package com.cshuig.patterns.javaApiClone;

/**
 * Created by hogan on 2015/8/12.
 */
public class HumanFactoryImpl implements HumanFactory {

    private American american;
    private Chinese chinese;

    public HumanFactoryImpl() {

    }

    public HumanFactoryImpl(American _american, Chinese _chinese) {
        this.american = _american;
        this.chinese = _chinese;
    }

    /**
     * deep copy
     * @return
     */
    @Override
    public American createAmerican() {
        try {
            return (American) american.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * shallow copy
     * @return
     */
    @Override
    public Chinese createChinese() {
        try {
            return (Chinese) chinese.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}
