package com.cshuig.patterns.javaApiClone;

import java.util.List;

/**
 * Created by hogan on 2015/8/13.
 */
public class ZhangFeiChinese extends Chinese {

    public ZhangFeiChinese() {

    }

    public ZhangFeiChinese(String _name, List<String> _list) {
        this.name = _name;
        this.list = _list;
    }

    @Override
    public Chinese clone() throws CloneNotSupportedException {
        return (Chinese) super.clone();
    }

    @Override
    public String toString() {
        return "ZhangFeiChinese:" + name;
    }
}
