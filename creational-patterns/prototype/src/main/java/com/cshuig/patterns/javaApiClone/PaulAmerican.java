package com.cshuig.patterns.javaApiClone;

import java.util.ArrayList;

/**
 * Created by hogan on 2015/8/12.
 */
public class PaulAmerican extends American {

    public PaulAmerican() {

    }

    public PaulAmerican(String _name, ArrayList<String> _list) {
        this.name = _name;
        this.list = _list;
    }
    @Override
    public American clone() throws CloneNotSupportedException {
        PaulAmerican paulAmerican = (PaulAmerican) super.clone();
        paulAmerican.list = (ArrayList<String>) this.list.clone();
        return paulAmerican;
    }

    @Override
    public String toString() {
        return "PaulAmerican:" + name;
    }

}
