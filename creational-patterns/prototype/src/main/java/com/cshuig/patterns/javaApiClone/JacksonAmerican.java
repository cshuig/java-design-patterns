package com.cshuig.patterns.javaApiClone;

import java.util.ArrayList;

/**
 * Created by hogan on 2015/8/12.
 */
public class JacksonAmerican extends American {

    public JacksonAmerican() {
        System.out.println("执行无参数构造函数");
    }

    public JacksonAmerican(String _name, ArrayList<String> _list) {
        this.name = _name;
        this.list = _list;
    }

    @Override
    public American clone() throws CloneNotSupportedException {
        JacksonAmerican jacksonAmerican = (JacksonAmerican) super.clone();
        jacksonAmerican.list = (ArrayList<String>) this.list.clone();
        return jacksonAmerican;
    }

    @Override
    public String toString() {
        return name;
    }

}
