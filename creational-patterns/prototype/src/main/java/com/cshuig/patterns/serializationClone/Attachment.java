package com.cshuig.patterns.serializationClone;

import java.io.Serializable;

/**
 * Created by hogan on 2015/8/13.
 */
public class Attachment implements Serializable {
    private String name;
    private Integer size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
