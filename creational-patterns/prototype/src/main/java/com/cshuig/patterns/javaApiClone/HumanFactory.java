package com.cshuig.patterns.javaApiClone;

/**
 * Created by hogan on 2015/8/12.
 */
public interface HumanFactory {

    American createAmerican();
    Chinese createChinese();
}
