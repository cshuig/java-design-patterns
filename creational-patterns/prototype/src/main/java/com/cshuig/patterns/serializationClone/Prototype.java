package com.cshuig.patterns.serializationClone;

import java.io.Serializable;

/**
 * Created by hogan on 2015/8/13.
 */
public abstract class Prototype implements Serializable {

    public abstract Object deepClone();

}
