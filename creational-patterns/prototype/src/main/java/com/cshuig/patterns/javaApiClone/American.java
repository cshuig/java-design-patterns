package com.cshuig.patterns.javaApiClone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hogan on 2015/8/12.
 */
public abstract class American extends Prototype {

    protected String name;
    protected ArrayList<String> list = new ArrayList<>();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }
}
