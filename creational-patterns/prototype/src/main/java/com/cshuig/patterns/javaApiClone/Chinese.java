package com.cshuig.patterns.javaApiClone;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hogan on 2015/8/12.
 */
public abstract class Chinese extends Prototype {

    protected String name;
    protected List<String> list = new ArrayList<>();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }
}
