package com.cshuig.factoryMethod;

import com.cshuig.Arms;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class App {

    public static void main(String[] args) {
        Factory sealFactory = new SEALFactory();
        Factory landFactory = new LandFactory();
        Factory airFactory = new AirFactory();

        Arms sealArms = sealFactory.createArms();
        Soldier sealSoldier = sealFactory.createSoldier();
        System.out.println(sealArms.toString());
        System.out.println(sealSoldier.toString());


        Arms landArms = landFactory.createArms();
        Soldier landSoldier = landFactory.createSoldier();
        System.out.println(landArms.toString());
        System.out.println(landSoldier.toString());

        Arms airArms = airFactory.createArms();
        Soldier airSoldier = airFactory.createSoldier();
        System.out.println(airArms.toString());
        System.out.println(airSoldier.toString());
    }
}
