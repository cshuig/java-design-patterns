package com.cshuig.factoryMethod;

import com.cshuig.Arms;
import com.cshuig.LandArms;
import com.cshuig.LandSoldier;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class LandFactory implements Factory {
    @Override
    public Arms createArms() {
        return new LandArms();
    }

    @Override
    public Soldier createSoldier() {
        return new LandSoldier();
    }
}
