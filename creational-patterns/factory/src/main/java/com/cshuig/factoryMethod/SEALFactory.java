package com.cshuig.factoryMethod;

import com.cshuig.Arms;
import com.cshuig.SEALArms;
import com.cshuig.SEALSoldier;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class SEALFactory implements Factory {

    @Override
    public Arms createArms() {
        return new SEALArms();
    }

    @Override
    public Soldier createSoldier() {
        return new SEALSoldier();
    }
}
