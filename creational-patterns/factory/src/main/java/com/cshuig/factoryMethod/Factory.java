package com.cshuig.factoryMethod;

import com.cshuig.Arms;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public interface Factory {

    public Arms createArms();

    public Soldier createSoldier();
}
