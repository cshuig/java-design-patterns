package com.cshuig.factoryMethod;

import com.cshuig.AirArms;
import com.cshuig.AirSoldier;
import com.cshuig.Arms;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class AirFactory implements Factory {
    @Override
    public Arms createArms() {
        return new AirArms();
    }

    @Override
    public Soldier createSoldier() {
        return new AirSoldier();
    }
}
