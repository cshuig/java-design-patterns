package com.cshuig;

/**
 * Created by cshuig on 15/6/21.
 * 陆战队士兵
 */
public class LandSoldier implements Soldier {
    @Override
    public String toString() {
        return "This is Land Soldier";
    }
}
