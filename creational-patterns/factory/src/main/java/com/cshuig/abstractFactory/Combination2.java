package com.cshuig.abstractFactory;

import com.cshuig.Arms;
import com.cshuig.LandArms;
import com.cshuig.SEALSoldier;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class Combination2 extends AbstractFactory {
    @Override
    public Arms createArms() {
        return new LandArms();
    }

    @Override
    public Soldier createSoldier() {
        return new SEALSoldier();
    }

    public void dosomething() {
        System.out.println(this.createArms().toString());
        System.out.println(this.createSoldier().toString());
    }
}
