package com.cshuig.abstractFactory;

import com.cshuig.AirArms;
import com.cshuig.Arms;
import com.cshuig.LandSoldier;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class Combination1 extends AbstractFactory {
    @Override
    public Arms createArms() {
        return new AirArms();
    }

    @Override
    public Soldier createSoldier() {
        return new LandSoldier();
    }

    public void dosomething() {
        System.out.println(this.createArms().toString());
        System.out.println(this.createSoldier().toString());
    }
}
