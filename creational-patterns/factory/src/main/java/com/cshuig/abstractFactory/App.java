package com.cshuig.abstractFactory;

/**
 * Created by cshuig on 15/6/21.
 */
public class App {

    public static void main(String[] args) {
        Combination1 combination1 = new Combination1();
        combination1.dosomething();

        Combination2 combination2 = new Combination2();
        combination2.dosomething();
    }
}
