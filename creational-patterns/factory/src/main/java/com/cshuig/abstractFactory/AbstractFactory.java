package com.cshuig.abstractFactory;

import com.cshuig.Arms;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public abstract class AbstractFactory {

    public abstract Arms createArms();

    public abstract Soldier createSoldier();
}
