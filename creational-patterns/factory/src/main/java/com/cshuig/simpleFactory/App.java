package com.cshuig.simpleFactory;

import com.cshuig.Arms;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 */
public class App {

    public static void main(String[] args) {

        Arms sealArms = TroopFactory.createSEALArms();
        Soldier sealSoldier = TroopFactory.createSEALSolider();
        System.out.println(sealArms.toString());
        System.out.println(sealSoldier.toString());

        Arms landArms = TroopFactory.createLandArms();
        Soldier landSoldier = TroopFactory.createLandSoldier();
        System.out.println(landArms.toString());
        System.out.println(landSoldier.toString());

        Arms airArms = TroopFactory.createAirArms();
        Soldier airSoldier = TroopFactory.createAirSoldier();
        System.out.println(airArms.toString());
        System.out.println(airSoldier.toString());
    }
}
