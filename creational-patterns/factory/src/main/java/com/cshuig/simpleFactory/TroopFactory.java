package com.cshuig.simpleFactory;


import com.cshuig.AirArms;
import com.cshuig.AirSoldier;
import com.cshuig.Arms;
import com.cshuig.LandArms;
import com.cshuig.LandSoldier;
import com.cshuig.SEALArms;
import com.cshuig.SEALSoldier;
import com.cshuig.Soldier;

/**
 * Created by cshuig on 15/6/21.
 * 队伍工厂
 */
public class TroopFactory {

    public static Arms createSEALArms() {
        return new SEALArms();
    }

    public static Soldier createSEALSolider() {
        return new SEALSoldier();
    }
    /** 组建一支海豹突击队完成 */

    public static Arms createLandArms() {
        return new LandArms();
    }

    public static Soldier createLandSoldier() {
        return new LandSoldier();
    }
    /** 组建一支陆战队完成 */


    public static Arms createAirArms() {
        return new AirArms();
    }

    public static Soldier createAirSoldier() {
        return new AirSoldier();
    }
    /** 组建一支空军完成 */

}
