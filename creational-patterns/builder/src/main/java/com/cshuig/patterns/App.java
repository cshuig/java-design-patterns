package com.cshuig.patterns;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        Persion persion = new Persion.Builder(1, "张飞").hairColor(HairColor.BLACK).hairType(HairType.BALD).build();
        System.out.println(persion.toString());
    }
}
