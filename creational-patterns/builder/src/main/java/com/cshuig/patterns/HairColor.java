package com.cshuig.patterns;

/**
 * Created by hogan on 2015/8/10.
 */
public enum HairColor {

    WHITE, BLOND, RED, BLACK;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
