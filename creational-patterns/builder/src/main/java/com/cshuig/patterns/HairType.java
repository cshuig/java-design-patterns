package com.cshuig.patterns;

/**
 * Created by hogan on 2015/8/10.
 */
public enum HairType {
    BALD("bald"),
    SHORT("short"),
    CURLY("curly"),
    LONG_STRAIGHT("long straight"),
    LONG_CURLY("long curly");

    private String title;

    HairType(String _title) {
        this.title = _title;
    }
}
