package com.cshuig.patterns;

/**
 * Created by hogan on 2015/8/10.
 */
public class Persion {
    private final Integer id;
    private final String name;
    private final Integer height;
    private final HairColor hairColor;
    private final HairType hairType;

    private Persion(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.height = builder.height;
        this.hairColor = builder.hairColor;
        this.hairType = builder.hairType;
    }
    public Integer getId() {
        return id;
    }

    public Integer getHeight() {
        return height;
    }

    public HairColor getHairColor() {
        return hairColor;
    }

    public HairType getHairType() {
        return hairType;
    }

    public String getName() {
        return name;
    }

    public static class Builder {
        private Integer id;
        private String name;

        // default value
        private Integer height = 100;
        private HairColor hairColor = HairColor.RED;
        private HairType hairType = HairType.CURLY;

        public Builder (Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public Builder height(Integer height) {
            this.height = height;
            return this;
        }

        public Builder hairColor(HairColor hairColor) {
            this.hairColor = hairColor;
            return this;
        }

        public Builder hairType(HairType hairType) {
            this.hairType = hairType;
            return this;
        }

        public Persion build() {
            return new Persion(this);
        }
    }

    @Override
    public String toString() {
        return "Persion{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", hairColor=" + hairColor +
                ", hairType=" + hairType +
                '}';
    }
}
